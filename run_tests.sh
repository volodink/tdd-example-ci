#!/bin/bash

rm -rf .venv
clear
tree
python -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
pip freeze
pip install -e .
rm -rf .venv
rm -rf tests/__pycache__
rm -rf src/app/__pycache__
rm -rf *.egg_info

